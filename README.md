# README #

Version 1


EZ_Utility Pooling is a robust and lightweight pooling system for unity built for optimizing games that uses alot of objects over and over again.

Objectives :
By managing a pool of instances and recycling objects that are not in use at that point of time, we can save on CPU overhead & memory that are required to instantiate & kill the objects.
So, pool manager can enhance performance when we are required to work with huge amount of objects that are needed frequently but only for a short amount of time.

Applications :
Spawning of bullets, particle effects, decals...


### How do I set up Pooling ? ###

1. Make sure the prefab you are pooling has the Pooled.cs
2. To spawn using pooling, simply replace your Instantiate method with PoolManager.Spawn and it will automatically create the pool if it does not already exists
3. There are 2 ways to despawn. one is to simply call FastDespawn at the object itself, but for that you need the reference to the Pooled.cs class. Another method is by using a death timer. You also need a reference to the Pooled.cs class.

There is 2 examples attached. One is a tower defense scene to demonstrate the usage of the pooling system.
Another is a stress test