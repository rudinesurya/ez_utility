﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZ_Utility;


public class Spam : MonoBehaviour
{
    public Pooled sphere;
    public Pool iKnowMyPool;

    float timeTaken1 = 0;
    float timeTaken2 = 0;


    private void OnGUI()
    {
        float timestamp = 0;
        
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Spam 1000 spheres"))
        {
            timestamp = Time.realtimeSinceStartup;
            for (int i = 0; i < 1000; ++i)
            {
                PoolManager.Spawn(sphere, Random.insideUnitSphere).SetDeathTimer(1);
            }
            timeTaken1 = Time.realtimeSinceStartup - timestamp;
        }
        GUILayout.Box("Spawn normally from a prefab");
        GUILayout.Box(string.Format("{0}", timeTaken1));
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Spam 1000 boxes"))
        {
            timestamp = Time.realtimeSinceStartup;
            for (int i = 0; i < 1000; ++i)
            {
                iKnowMyPool.FastSpawn(Random.insideUnitSphere).SetDeathTimer(1);
            }
            timeTaken2 = Time.realtimeSinceStartup - timestamp;
        }
        GUILayout.Box("Spawn using the pool reference. This is faster");
        GUILayout.Box(string.Format("{0}", timeTaken2));
        GUILayout.EndHorizontal();
    }
}
