﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace EZ_Utility
{
    [System.Serializable]
    public struct PoolInfo
    {
        public int total;
        public int active;
    }

    public sealed class Pool : MonoBehaviour
    {
        //user settings
        public Pooled master;
        public int preload;
        public PoolInfo poolInfo;
        public bool showDebugLog;
        
        public Stack<Pooled> cache = new Stack<Pooled>();

        private void Awake()
        {
            PreloadPool();
        }
        
        #region To Handle Flexible parameters
        public Pooled FastSpawn(Vector3 position)
        {
            return FastSpawn(position, Constants.quarternionIdentity, null);
        }

        public Pooled FastSpawn(Vector3 position, Quaternion rotation)
        {
            return FastSpawn(position, rotation, null);
        }
        #endregion


        public Pooled FastSpawn(Vector3 position, Quaternion rotation, Transform parent)
        {
            if (cache.Count == 0)
            {
                // Instantiate a new one
                var newItem = Transform.Instantiate(master.transform);
                ++(poolInfo.total);
                var newActor = newItem.GetComponent<Pooled>();
                newActor.Initialize();
                newActor.myPool = this;
                newActor.cachedGameObject.SetActive(false);
                newActor.cachedGameObject.hideFlags = HideFlags.HideInHierarchy;
                cache.Push(newActor);

                return FastSpawn(position, rotation, parent);
            }
            else
            {
                var freeObject = cache.Pop();

                freeObject.cachedTransform.position = position;
                freeObject.cachedTransform.rotation = rotation;
                freeObject.cachedTransform.parent = parent;
                freeObject.cachedGameObject.SetActive(true);
                
                freeObject.OnSpawned();
                ++(poolInfo.active);

                return freeObject;
            }
        }


        //Despawning
        public void FastDespawn(Pooled victim)
        {
            if (victim != null)
            {
                cache.Push(victim);
                victim.OnDespawned();
                --(poolInfo.active);
                victim.cachedGameObject.SetActive(false);
            }
        }


        public void PreloadPool()
        {
            if (master != null)
            {
                poolInfo.total = preload;
                Transform clone = master.transform;
                //preload
                for (int i = 0; i < preload; ++i)
                {
                    var newItem = Transform.Instantiate(clone);
                    var newActor = newItem.GetComponent<Pooled>();

                    newActor.Initialize();
                    newActor.myPool = this;
                    newActor.cachedGameObject.SetActive(false);
                    newActor.cachedGameObject.hideFlags = HideFlags.HideInHierarchy;
                    cache.Push(newActor);
                }
            }
        }
    }
}
