﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

namespace EZ_Utility
{
    [InitializeOnLoad]
    public class PoolManagerHierarchyIcon
    {
        private static readonly Texture2D icon;

        static PoolManagerHierarchyIcon()
        {
            icon = AssetDatabase.LoadAssetAtPath("Assets/EZ Utility/Pooling/Gizmos/PoolManager.png", typeof(Texture2D)) as Texture2D;

            EditorApplication.hierarchyWindowItemOnGUI += DrawIconOnWindowItem;
        }

        private static void DrawIconOnWindowItem(int instanceID, Rect rect)
        {
            GameObject gameObject = EditorUtility.InstanceIDToObject(instanceID) as GameObject;

            if (gameObject == null)
            {
                return;
            }

            var poolManager = gameObject.GetComponent<PoolManager>();
            if (poolManager != null)
            {
                if (icon == null)
                    return;

                float iconWidth = 15;
                EditorGUIUtility.SetIconSize(new Vector2(iconWidth, iconWidth));
                var padding = new Vector2(5, 0);
                var iconDrawRect = new Rect(
                                       rect.xMax - (iconWidth + padding.x),
                                       rect.yMin,
                                       rect.width,
                                       rect.height * 1.3f);
                var iconGUIContent = new GUIContent(icon);
                EditorGUI.LabelField(iconDrawRect, iconGUIContent);
                EditorGUIUtility.SetIconSize(Vector2.zero);
            }
        }
    }
}