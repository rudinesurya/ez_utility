﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
#if !(UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4)
using UnityEngine.AI;
#endif

namespace EZ_Utility
{
    [InitializeOnLoad]
    public class PoolHierarchyIcon
    {
        private static readonly Texture2D UIIcon;
        private static readonly Texture2D AIIcon;
        private static readonly Texture2D ParticleSystemIcon;
        private static readonly Texture2D LightIcon;
        private static readonly Texture2D AudioSourceIcon;
        private static readonly Texture2D DefaultIcon;

        static PoolHierarchyIcon()
        {
            UIIcon = AssetDatabase.LoadAssetAtPath("Assets/EZ Utility/Pooling/Gizmos/UI.png", typeof(Texture2D)) as Texture2D;
            AIIcon = AssetDatabase.LoadAssetAtPath("Assets/EZ Utility/Pooling/Gizmos/AI.png", typeof(Texture2D)) as Texture2D;
            ParticleSystemIcon = AssetDatabase.LoadAssetAtPath("Assets/EZ Utility/Pooling/Gizmos/ParticleSystem.png", typeof(Texture2D)) as Texture2D;
            LightIcon = AssetDatabase.LoadAssetAtPath("Assets/EZ Utility/Pooling/Gizmos/Light.png", typeof(Texture2D)) as Texture2D;
            AudioSourceIcon = AssetDatabase.LoadAssetAtPath("Assets/EZ Utility/Pooling/Gizmos/AudioSource.png", typeof(Texture2D)) as Texture2D;
            DefaultIcon = AssetDatabase.LoadAssetAtPath("Assets/EZ Utility/Pooling/Gizmos/Default.png", typeof(Texture2D)) as Texture2D;

            EditorApplication.hierarchyWindowItemOnGUI += DrawIconOnWindowItem;
        }

        private static void DrawIconOnWindowItem(int instanceID, Rect rect)
        {
            GameObject gameObject = EditorUtility.InstanceIDToObject(instanceID) as GameObject;

            if (gameObject == null)
            {
                return;
            }
            
            var pool = gameObject.GetComponent<Pool>();
            if (pool != null)
            {
                GameObject masterObj = pool.master.gameObject;
                if (masterObj == null)
                    return;

                Texture2D icon;
                //Determine which icon to use, the higher the more priority
                if (masterObj.GetComponent<Text>() != null || masterObj.GetComponent<Image>() != null)
                {
                    icon = UIIcon;
                }
                
                else if (masterObj.GetComponent<NavMeshAgent>() != null)
                {
                    icon = AIIcon;
                }
                else if (masterObj.GetComponent<ParticleSystem>() != null)
                {
                    icon = ParticleSystemIcon;
                }
                else if (masterObj.GetComponent<Light>() != null)
                {
                    icon = LightIcon;
                }
                else if (masterObj.GetComponent<AudioSource>() != null)
                {
                    icon = AudioSourceIcon;
                }
                else
                {
                    icon = DefaultIcon;
                }

                if (icon == null)
                    return;

                float iconWidth = 15;
                EditorGUIUtility.SetIconSize(new Vector2(iconWidth, iconWidth));
                var padding = new Vector2(5, 0);
                var iconDrawRect = new Rect(
                                       rect.xMax - (iconWidth + padding.x),
                                       rect.yMin,
                                       rect.width,
                                       rect.height*1.3f);
                var iconGUIContent = new GUIContent(icon);
                EditorGUI.LabelField(iconDrawRect, iconGUIContent);
                EditorGUIUtility.SetIconSize(Vector2.zero);
            }
        }
    }
}