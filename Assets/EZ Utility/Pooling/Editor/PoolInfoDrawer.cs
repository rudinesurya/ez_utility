﻿using UnityEngine;
using UnityEditor;

namespace EZ_Utility
{
    [CustomPropertyDrawer(typeof(PoolInfo))]
    public class PoolInfoDrawer : PropertyDrawer
    {
        // Draw the property inside the given rect
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // Using BeginProperty / EndProperty on the parent property means that
            // prefab override logic works on the entire property.
            EditorGUI.BeginProperty(position, label, property);

            // Draw label
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            // Don't make child fields be indented
            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            // Calculate rects
            var rect = new Rect(position.x, position.y, position.width, position.height);

            int active = property.FindPropertyRelative("active").intValue;
            int total = property.FindPropertyRelative("total").intValue;
            var labelx = string.Format("{0} / {1}", active, total);
            
            EditorGUI.ProgressBar(rect, (float)active / total, labelx);

            // Set indent back to what it was
            EditorGUI.indentLevel = indent;

            EditorGUI.EndProperty();
        }
    }
}