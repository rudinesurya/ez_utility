﻿using System;
using UnityEngine;
using UnityEngine.Events;


namespace EZ_Utility
{
    public sealed class Pooled : MonoBehaviour
    {
        [HideInInspector]
        public GameObject cachedGameObject;
        [HideInInspector]
        public Transform cachedTransform;
        [HideInInspector]
        public Pool myPool;

        public Action onSpawned;
        public Action onDespawned;

        private StopWatch stopWatch = new StopWatch();


        private void Awake()
        {
            stopWatch.OnComplete += FastDespawn;
        }


        private void Update()
        {
            stopWatch.Update(Time.deltaTime);
        }


        //Initialize will be called when the object is created by the pool manager. 
        //It will not be called during OnSpawned or in Unity's Awake / Start methods.
        public void Initialize()
        {
            cachedGameObject = this.gameObject;
            cachedTransform = this.transform;
        }

        /// <summary>
        /// This function will be called from the pool manager upon spawn
        /// </summary>
        public void OnSpawned()
        {
            stopWatch.Pause(true);

            if (onSpawned != null)
                onSpawned();
        }

        /// <summary>
        /// This function will be called from the pool manager upon despawn
        /// </summary>
        public void OnDespawned()
        {
            stopWatch.Pause(true);

            if (onDespawned != null)
                onDespawned();
        }


        public void FastDespawn()
        {
            if (myPool != null)
                myPool.FastDespawn(this);
            else
            {
                Debug.LogWarningFormat("Failed to despawn {0} which is not in the pool", this.gameObject.name);
                Destroy(this.gameObject);
            }
        }


        public Pooled SetDeathTimer(float t)
        {
            stopWatch.Reset(t);
            stopWatch.Pause(false);
            return this;
        }
    }
}