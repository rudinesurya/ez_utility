﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace EZ_Utility
{
    public sealed class PoolManager : MonoBehaviour
    {
        private static PoolManager instance;

        public static Dictionary<Pooled, Pool> AllLinks = new Dictionary<Pooled, Pool>();

        
        #region To Handle Flexible parameters
        public static Pooled Spawn(Pooled newGuy, Vector3 position)
        {
            return Spawn(newGuy, position, Constants.quarternionIdentity, null);
        }

        public static Pooled Spawn(Pooled newGuy, Vector3 position, Quaternion rotation)
        {
            return Spawn(newGuy, position, rotation, null);
        }
        #endregion
        

        private static void GetLinks()
        {
            var pools = instance.gameObject.GetComponentsInChildren<Pool>();
            for (int i = 0; i < pools.Length; ++i)
            {
                AllLinks.Add(pools[i].master, pools[i]);
            }
        }


        public static Pooled Spawn(Pooled newGuy, Vector3 position, Quaternion rotation, Transform parent)
        {
            //Restart the poolmanager during change of scene
            if (instance == null)
            {
                instance = FindObjectOfType<PoolManager>();
                AllLinks.Clear();

                if (instance == null)
                {
                    instance = new GameObject("Pool Manager").AddComponent<PoolManager>();
                }
                else
                {
                    GetLinks(); //repopulate the AllLinks with links that existed in the new poolmanager
                }
            }

            if (newGuy != null)
            {
                //try to find the pool
                Pool thePool;
                if (AllLinks.TryGetValue(newGuy, out thePool))
                {
                }
                else
                {
                    thePool = new GameObject(newGuy.name + " Pool").AddComponent<Pool>();
                    thePool.transform.parent = instance.transform;
                    thePool.master = newGuy;
                    thePool.master.Initialize();

                    AllLinks.Add(newGuy, thePool);
                }

                return thePool.FastSpawn(position, rotation, parent);
            }

            return null;
        }
    }
}