﻿using System.Collections.Generic;


namespace EZ_Utility
{
    [System.Serializable]
    public class PhysicsLayerConstraint
    {
        public string layer;
        public List<string> ignore = new List<string>();
    }
}