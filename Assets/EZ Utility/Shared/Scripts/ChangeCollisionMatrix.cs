﻿using System.Collections.Generic;
using UnityEngine;


namespace EZ_Utility
{
    [CreateAssetMenu()]
    public class ChangeCollisionMatrix : ScriptableObject
    {
        public List<string> allLayer = new List<string>();
        public List<PhysicsLayerConstraint> rules = new List<PhysicsLayerConstraint>();


        public void ApplyChanges()
        {
            //Restart matrix
            for (int i = 0; i < allLayer.Count; i++)
            {
                for (int j = 0; j < allLayer.Count; j++)
                {
                    Physics.IgnoreLayerCollision(LayerMask.NameToLayer(allLayer[i]), LayerMask.NameToLayer(allLayer[j]), false);
                }
            }

            //Ignore self
            for (int i = 0; i < rules.Count; i++)
            {
                string myLayer = rules[i].layer;
                for (int j = 0; j < rules[i].ignore.Count; j++)
                {
                    Physics.IgnoreLayerCollision(LayerMask.NameToLayer(myLayer), LayerMask.NameToLayer(rules[i].ignore[j]));
                }
            }
        }
    }
}
