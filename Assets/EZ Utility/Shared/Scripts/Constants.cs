﻿using UnityEngine;


namespace EZ_Utility
{
    public static class Constants
    {
        //vec3
        public static Vector3 vec3Zero = Vector3.zero;
        public static Vector3 vec3One = Vector3.one;
        public static Vector3 vec3Infinity = new Vector3(float.PositiveInfinity, float.PositiveInfinity, float.PositiveInfinity);
        public static Vector3 vec3Left = Vector3.left;
        public static Vector3 vec3Right = Vector3.right;
        public static Vector3 vec3Forward = Vector3.forward;
        public static Vector3 vec3Back = Vector3.back;
        public static Vector3 vec3Up = Vector3.up;

        //quaternion
        public static Quaternion quarternionIdentity = Quaternion.identity;
    }
}