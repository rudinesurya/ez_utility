using UnityEngine;


namespace EZ_Utility
{
    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        /// <summary>
        /// Returns the instance of this singleton.
        /// </summary>
        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = (T)FindObjectOfType(typeof(T));

                    if (instance == null)
                    {
                        Debug.LogErrorFormat("Failed to find Singleton of type : {0} ", typeof(T));
                    }
                }

                return instance;
            }
        }

        protected static T instance;
    }
}