﻿using System;


namespace EZ_Utility
{
	public sealed class StopWatch
	{
        public event Action OnComplete;


        public bool IsPaused
        {
            get; private set;
        }


        public bool IsDone
        {
            get; private set;
        }


        public float Time
        {
            get; private set;
        }


        public StopWatch()
        {
            IsPaused = true;
            Reset(0);
        }


        public void Update(float deltaTime)
        {
            if (IsPaused || IsDone)
                return;

            Time -= deltaTime;
            CheckIfCountdownEnded();
        }


        public void AddTime(float timeIncrement)
        {
            Time += timeIncrement;
        }


        public void Reset(float startTime)
        {
            Time = startTime;
            IsDone = false;
        }


        public void Pause(bool val)
        {
            IsPaused = val;
        }


        private void CheckIfCountdownEnded()
		{
			if (Time <= 0)
			{
				Time = 0;
				IsDone = true;

				if (OnComplete != null)
				{
					OnComplete();
				}
			}
		}
	}
}