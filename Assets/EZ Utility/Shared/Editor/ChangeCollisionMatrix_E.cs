﻿using UnityEngine;
using UnityEditor;


namespace EZ_Utility
{
    [CustomEditor(typeof(ChangeCollisionMatrix))]
    public class ChangeCollisionMatrix_E : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            GUILayout.Space(20);

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Apply", GUILayout.Width(100), GUILayout.Height(50)))
            {
                ((ChangeCollisionMatrix)target).ApplyChanges();
            }

            if (GUILayout.Button("Open Physics", GUILayout.Width(100), GUILayout.Height(50)))
            {
                EditorApplication.ExecuteMenuItem("Edit/Project Settings/Physics");
            }
            GUILayout.EndHorizontal();
        }
    }
}