﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sight : MonoBehaviour
{
    public Transform eye;
    public float viewDistance;
    public LayerMask targetLayer;
    
    private Transform cachedTransform;
    private float sqrViewDistance;


    private void Awake()
    {
        cachedTransform = transform;
        sqrViewDistance = viewDistance * viewDistance;
    }


    public Transform GetClosestTarget()
    {
        int n = Physics.OverlapSphereNonAlloc(transform.position, viewDistance, GlobalVar.container, targetLayer);

        int indexOfClosestTarget = -1;
        float closestSqrDistance = Mathf.Infinity;
        for (int i = 0; i < n; ++i)
        {
            float sqrMagnitude = (GlobalVar.container[i].transform.position - transform.position).sqrMagnitude;
            if (sqrMagnitude < closestSqrDistance)
            {
                indexOfClosestTarget = i;
                closestSqrDistance = sqrMagnitude;
            }
        }

        if (indexOfClosestTarget != -1)
        {
            return GlobalVar.container[indexOfClosestTarget].transform;
        }

        return null;
    }


    private void OnDrawGizmos()
    {
        RadiusGizmo();
    }

    #region Gizmos
    private void RadiusGizmo()
    {
#if UNITY_EDITOR
        var oldColor = UnityEditor.Handles.color;
        var color = Color.yellow;
        color.a = 0.1f;
        UnityEditor.Handles.color = color;
        UnityEditor.Handles.DrawSolidDisc(eye.position, EZ_Utility.Constants.vec3Up, viewDistance);
        UnityEditor.Handles.color = oldColor;
#endif
    }
    #endregion
}
