﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZ_Utility;


public abstract class Projectile : MonoBehaviour
{
    //properties of the projectile
    protected float damage = 10;
    protected float speed = 2;
    protected float maxRange = 4;
    protected string targetTag;

    protected Pooled pooled;
    protected Transform cachedTransform;

    
    protected virtual void Awake()
    {
        cachedTransform = transform;
        pooled = GetComponent<Pooled>();
    }

    
    protected virtual void SelfDestruct()
    {
        pooled.FastDespawn();
    }
}
