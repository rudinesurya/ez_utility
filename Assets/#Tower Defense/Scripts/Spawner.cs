﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZ_Utility;

public class Spawner : MonoBehaviour
{
    public int spawnAmount = 1;
    public float delayBeforeFirstSpawn;
    public float spawnTime;
    public Pooled unitPrefab;

    private StopWatch stopWatch = new StopWatch();

    private void Awake()
    {
        stopWatch.OnComplete += Spawn;
        stopWatch.Reset(delayBeforeFirstSpawn);
        stopWatch.Pause(false);
    }

    private void Update()
    {
        stopWatch.Update(Time.deltaTime);
    }

    private void Spawn()
    {
        for (int i = 0; i < spawnAmount; ++i)
        {
            PoolManager.Spawn(unitPrefab, transform.position);
        }

        stopWatch.Reset(spawnTime);
    }
}
