﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PhysicProjectile : Projectile
{
    public Ease easeType = Ease.Linear;
    
    private float tweenDuration;


    public virtual void Launch(float damage, float speed, float maxRange, string targetTag)
    {
        this.damage = damage;
        this.speed = speed;
        this.maxRange = maxRange;
        this.targetTag = targetTag;
        tweenDuration = maxRange / speed;

        cachedTransform.DOKill();
        Vector3 destination = cachedTransform.position + cachedTransform.forward * maxRange;
        cachedTransform.DOMove(destination, tweenDuration).SetEase(easeType).OnComplete(SelfDestruct);
    }


    protected virtual void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(targetTag))
        {
            var targetHealth = other.GetComponent<IHealth>();
            if (targetHealth != null)
                targetHealth.TakeDamage(damage);

            SelfDestruct();
        }
    }
}
