﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackOnSight : MonoBehaviour
{
    private Sight sight;
    private IAttack attack;
    private Transform target;
    private Transform cachedTransform;
    private Health targetHealth;

    private void Awake()
    {
        cachedTransform = transform;
        sight = GetComponent<Sight>();
        attack = GetComponent<IAttack>();
    }

    private void Update()
    {
        if (target == null)
        {
            target = sight.GetClosestTarget();
            if (target != null)
                targetHealth = target.GetComponent<Health>();
        }
        else if (targetHealth.IsAlive() == false || attack.InAttackRange(target.position) == false)
        {
            target = null;
        }
        
        if (target != null && attack.CanAttack())
            attack.Attack(target);
    }
}
