﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuidedProjectile : Projectile
{
    private Transform target;
    private IHealth targetHealth;
    private Vector3 lastSeenPosition;

    protected override void Awake()
    {
        base.Awake();
    }

    public virtual void Launch(Transform target, float damage, float speed, float maxRange, string targetTag)
    {
        this.target = target;
        this.damage = damage;
        this.speed = speed;
        this.maxRange = maxRange;
        this.targetTag = targetTag;
        this.targetHealth = target.GetComponent<IHealth>();
    }

    private void Update()
    {
        if (target != null && targetHealth.IsAlive())
        {
            lastSeenPosition = target.position;
        }
        else
        {
            target = null;
            targetHealth = null;
        }
        
        Vector3 vec = lastSeenPosition - cachedTransform.position;
        if (vec.magnitude < 0.1f)
        {
            if (targetHealth != null)
                targetHealth.TakeDamage(damage);
            SelfDestruct();
        }
        else
        {
            cachedTransform.rotation = Quaternion.LookRotation(vec);
            cachedTransform.position += cachedTransform.forward * speed * Time.deltaTime;
        }
    }
}
