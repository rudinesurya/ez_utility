﻿using System;
using UnityEngine;
using EZ_Utility;

public class Health : MonoBehaviour, IHealth
{
    public float startHealth = 100;
    private float currentHealth;
    private Pooled pooled;

    private void Awake()
    {
        pooled = GetComponent<Pooled>();
        if (pooled != null)
            pooled.onSpawned += ResetHealth;
        currentHealth = startHealth;
    }

    public bool IsAlive()
    {
        return currentHealth > 0;
    }

    public void TakeDamage(float amount)
    {
        currentHealth = Mathf.Max(currentHealth - amount, 0);

        if (currentHealth == 0)
        {
            if (pooled != null)
                pooled.FastDespawn();
            else
                Destroy(gameObject);
        }
    }

    void ResetHealth()
    {
        currentHealth = startHealth;
    }
}