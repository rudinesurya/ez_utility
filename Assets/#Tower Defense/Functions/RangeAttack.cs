﻿using System;
using UnityEngine;
using EZ_Utility;

public class RangeAttack : MonoBehaviour, IAttack
{
    public Pooled projectilePrefab;
    //properties of the projectile
    public float damage = 10;
    public float speed = 2;
    public float maxRange = 4;
    public string targetTag;

    public Transform attackPoint;
    public float attackDistance = 5;
    public float attackCooldown = 1;

    private Transform cachedTransform;
    private float previousAttack;

    private void Awake()
    {
        cachedTransform = transform;
        previousAttack = -attackCooldown;
    }

    
    public bool CanAttack()
    {
        return (previousAttack + attackCooldown) < Time.time;
    }

    public void Attack(Vector3 targetPosition)
    {
        
    }

    public void Attack(Transform target)
    {
        var go = PoolManager.Spawn(projectilePrefab, attackPoint.position, Quaternion.LookRotation(target.position - attackPoint.position));
        go.GetComponent<GuidedProjectile>().Launch(target, damage, speed, maxRange, targetTag);
        previousAttack = Time.time;
    }

    public bool InAttackRange(Vector3 targetPosition)
    {
        return (targetPosition - cachedTransform.position).magnitude < attackDistance;
    }
}