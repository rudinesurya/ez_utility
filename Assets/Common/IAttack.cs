﻿using UnityEngine;

public interface IAttack
{
    bool InAttackRange(Vector3 targetPosition);
    bool CanAttack();
    void Attack(Vector3 targetPosition);
    void Attack(Transform target);
}