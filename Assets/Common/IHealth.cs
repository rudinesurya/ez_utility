﻿public interface IHealth
{
    bool IsAlive();
    void TakeDamage(float amount);
}